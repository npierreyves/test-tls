package fr.npierreyves.demo.tls.client;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ClientConfig {

    @Bean
    @Scope("prototype")
    public SSLContextHelper sslTrustManagerHelper(@Value("${client.ssl.one-way-authentication-enabled:false}") boolean oneWayAuthenticationEnabled,
                                                  @Value("${client.ssl.two-way-authentication-enabled:false}") boolean twoWayAuthenticationEnabled,
                                                  @Value("${client.ssl.key-store:}") String keyStorePath,
                                                  @Value("${client.ssl.key-store-password:}") String keyStorePassword,
                                                  @Value("${client.ssl.trust-store:}") String trustStorePath,
                                                  @Value("${client.ssl.trust-store-password:}") String trustStorePassword) {
        SSLContextHelper.Builder sslContextHelperBuilder = SSLContextHelper.builder();
        if (oneWayAuthenticationEnabled) {
            sslContextHelperBuilder.withOneWayAuthentication(trustStorePath, trustStorePassword)
                    .withHostnameVerifierEnabled(true);
        }

        if (twoWayAuthenticationEnabled) {
            sslContextHelperBuilder.withTwoWayAuthentication(keyStorePath, keyStorePassword, trustStorePath, trustStorePassword)
                    .withHostnameVerifierEnabled(true);
        }
        return sslContextHelperBuilder.build();
    }

    @Bean
    @Scope("prototype")
    public org.apache.http.client.HttpClient apacheHttpClient(SSLContextHelper sslContextHelper) {
        HttpClientBuilder httpClientBuilder = HttpClients.custom();
        if (sslContextHelper.isSecurityEnabled()) {
            httpClientBuilder.setSSLContext(sslContextHelper.getSslContext());
            httpClientBuilder.setSSLHostnameVerifier(sslContextHelper.getHostnameVerifier());
        }
        return httpClientBuilder.build();
    }

    @Bean
    public RestTemplate restTemplate(org.apache.http.client.HttpClient httpClient) {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }
}
