package fr.npierreyves.demo.tls.client;

import org.jline.terminal.Terminal;

public class ShellHelper {
    private Terminal terminal;

    public ShellHelper(Terminal terminal) {
        this.terminal = terminal;
    }

    public void print(String toPrint) {
        terminal.writer().println(toPrint);
        terminal.flush();
    }
}
