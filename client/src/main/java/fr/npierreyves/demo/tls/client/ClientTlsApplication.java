package fr.npierreyves.demo.tls.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientTlsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientTlsApplication.class, args);
	}

}
