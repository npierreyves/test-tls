package fr.npierreyves.demo.tls.client;

import org.springframework.http.HttpStatus;

public class ClientResponse {

    private String responseBody;
    private HttpStatus statusCode;

    public ClientResponse(String responseBody, HttpStatus statusCode) {
        this.responseBody = responseBody;
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

}
