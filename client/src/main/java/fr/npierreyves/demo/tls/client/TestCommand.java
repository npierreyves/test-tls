package fr.npierreyves.demo.tls.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class TestCommand {
    private final ShellHelper shellHelper;
    private final RestTemplateWrapper restTemplateWrapper;

    @Autowired
    public TestCommand(RestTemplateWrapper restTemplateWrapper, ShellHelper shellHelper) {
        this.restTemplateWrapper = restTemplateWrapper;
        this.shellHelper = shellHelper;
    }

    @ShellMethod("Call test url")
    public void get(@ShellOption(value = {"-u", "--url"}, defaultValue = "https://localhost:8443/api/test/alive") String url) {
        ClientResponse clientResponse = restTemplateWrapper.executeRequest(url);
        if(HttpStatus.OK.equals(clientResponse.getStatusCode())) {
            shellHelper.print(clientResponse.getResponseBody());
        } else {
            shellHelper.print(clientResponse.getStatusCode().getReasonPhrase());
        }
    }

}
