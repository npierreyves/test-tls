

# TLS cas de test

Le but est de tester les différentes configuration de TLS entre un client RestTemplate et un serveur Java Spring Boot.
```
+--------+            +---------+
| Client |------------| Serveur |
+--------+            +---------+
```

1. Sans TLS - HTTP
2. Avec dec certificats autosignés
   1. TLS unidirectionnel - identification du serveur (one-way TLS)
   2. TLS bidirectionnel - identification du client (two-way TLS)
3. Avec des certificats signés par une AC
   1. TLS bidirectionnel - identification du client (two-way TLS)
   2. TLS bidirectionnel  - basé sur l'autorité de certification (two-way TLS)

##  Sans TLS - HTTP

### Serveur

#### Mise en place 

__application.yml__
 ```yaml
 server:
   port: 8080
   ssl:
     enabled: off
 ```

#### Test

```
curl -XGET -v http://localhost:8080/api/test/alive
```

### Client 

```
shell:>get http://localhost:8080/api/test/alive
```
## Avec dec certificats autosignés

### TLS unidirectionnel - identification du serveur (one-way TLS)

#### Serveur

##### Mise en place

Création d'un keystore avec une clef publique et une clef privée :
```
keytool -genkeypair -keyalg RSA -keysize 2048 -alias server -dname "CN=ubuntu,O=npierreyves,S=,C=FR" -validity 3650 -keystore "server/src/main/resources/server.jks" -storepass password -keypass password -deststoretype pkcs12
```

__application.yml__
 ```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server
    key-password: password
    client-auth: none
```

#### Test

```
curl -k -v https://localhost:8443/api/test/alive
```

#### Client 

```
shell:>get https://localhost:8443/api/test/alive
```

Le résultat sera : `PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target`.

##### Explication
Pendant l'étalissement de la communication HTTPS, le client reçoit un certificat de la part du serveur qu'il ne reconnait pas.

##### Solution
Il faut ajouter le certificat dans le trust store du client.
1. On va récupérer le certificat dans le keystore server avec la commande :

```
keytool -exportcert -keystore "server/src/main/resources/server.jks" -storepass password -alias server -rfc -file "server/src/main/resources/server.cer"
```

2. Ajouter le certificat dans un truststore pour le client avec la commande :

```
keytool -keystore client/src/main/resources/truststore.jks -importcert -file "server/src/main/resources/server.cer" -alias server -storepass password
```

3. Mettre au courant le client que le trustore existe :

__application.yml__

```yaml
client:
  ssl:
    one-way-authentication-enabled: true
    two-way-authentication-enabled: false
    trust-store: truststore.jks
    trust-store-password: password
```

4. refaire le test

```
shell:>get https://localhost:8443/api/test/alive
```

Le résultat sera : `Certificate for <localhost> doesn't match any of the subject alternative names: []`.
Mais avec la command : 

```
shell:>get https://ubuntu:8443/api/test/alive
```

Le résultat est : `I'm alive !`

L'émetteur du certificat est 'CN=ubuntu, O=npierreyves, ST=, C=FR'.
Le CN est au nom de 'ubuntu' par concéquent : 
1. lors de l'appel avec __localhost__ la recherche se fait sur le SAN.
2. lors de l'appel avec __ubuntu__ la recherche sur le CN du subject est OK.

Cette pratique ne semble pas bonne : [http://wiki.cacert.org/FAQ/subjectAltName](subjectAltName) :

> subjectAltName must always be used (RFC 3280 4.2.1.7, 1. paragraph). CN is only evaluated if subjectAltName is not present and only for compatibility with old, non-compliant software. So if you set subjectAltName, you have to use it for all host names, email addresses, etc., not just the "additional" ones.

5. Ajouter tout ce qu'il faut au niveau de la génération : 

 - [ ] On ajoute une nouvelle paire de clefs possédant une SAN adapté :

```
keytool -genkeypair -keyalg RSA -keysize 2048 -alias server_san -dname "CN=ubuntu,O=npierreyves,S=,C=FR" -ext "SAN=DNS:localhost,IP:127.0.0.1,IP:192.168.86.130" -validity 3650 -keystore "server/src/main/resources/server.jks" -storepass password -keypass password -deststoretype pkcs12
```
 - [ ] On modification la configuration serveur pour qu'il prenne le nouvelle clef :

__application.yml__

 ```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server_san
    key-password: password
    client-auth: none
```

 - [ ] On récupère le nouveau certificat :

```
keytool -exportcert -keystore "server/src/main/resources/server.jks" -storepass password -alias server_san -rfc -file "server/src/main/resources/server_san.cer"
```

- [ ] On l'injecte dans le trustore :

```
keytool -keystore client/src/main/resources/truststore.jks -importcert -file "server/src/main/resources/server_san.cer" -alias server_san -storepass password
```

- [ ] On relance client et serveur et on test :

```
shell:>get https://localhost:8443/api/test/alive
shell:>get https://127.0.0.1:8443/api/test/alive
shell:>get https://192.168.86.130:8443/api/test/alive
shell:>get https://ubuntu:8443/api/test/alive
```

Le résultat est : `I'm alive !` pour les 3 premiers test, pour le quatrième nous avons `Certificate for <ubuntu> doesn't match any of the subject alternative names: [localhost, 127.0.0.1, 192.168.86.130]`.
Si le SAN est présent alors le CN de l'émetteur n'est pas testé.
Donc le SAN doit contenir les IP et nom permettant d'accèder au service.

### TLS bidirectionnel - identification du client (two-way TLS)

#### Serveur

##### Mise en place 

__application.yml__

 ```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server
    key-password: password
    client-auth: need
```

##### Test

```
shell:>get https://localhost:8443/api/test/alive
```

Le résultat sera : `Received fatal alert: bad_certificate;`

##### Explication

Le certificat envoyé par le client n'est pas valide ... normal il n'y a pas de scertificat du tout !

##### Solution 

La configuration client doit permettre d'envoyer un certificat client. Voir la suite ...

#### Client

##### Mise en place

 - [ ] Création d'un keystore avec une clef publique et une clef privée :

```
keytool -genkeypair -keyalg RSA -keysize 2048 -alias client -dname "CN=ubuntu,O=npierreyves,C=FR" -validity 3650 -keystore "client/src/main/resources/client.jks" -storepass password -keypass password -deststoretype pkcs12
```

 - [ ] On va récupérer le certificat dans le keystore client avec la commande :

```
keytool -exportcert -keystore "client/src/main/resources/client.jks" -storepass password -alias client -rfc -file "client/src/main/resources/client.cer"
```

 - [ ] Configurer le client avec son nouveau keystore

__application.yml__

```yaml
client:
  ssl:
    one-way-authentication-enabled: false
    two-way-authentication-enabled: true
    key-store: client.jks
    key-store-password: password
    key-password: password
    trust-store: truststore.jks
    trust-store-password: password
```

 - [ ] Test 

```
shell:>get https://localhost:8443/api/test/alive
```

Le résultat sera : `Received fatal alert: certificate_unknown`

##### Explication

Le certificat envoyé par le client est bon mais inconnu par le serveur ... normal il n'est pas présent dans le keystore !

##### Solution

La configuration serveur doit permettre de valider ce certificat. Voir la suite ...

 - [ ] Ajouter le certificat dans un truststore pour le server avec la commande :

```
keytool -keystore server/src/main/resources/truststore.jks -importcert -file "client/src/main/resources/client.cer" -alias client -storepass password
```

__application.yml__

```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server_san
    key-password: password
    trust-store: classpath:truststore.jks
    trust-store-password: password
    client-auth: need
```

Puis relancer le serveur.

  - [ ] Test 2

 ```
 shell:>get https://localhost:8443/api/test/alive
 ```

Le résulat est : `I'm alive !`

__La totalité des tests précédents ont été réalisée à partie de certificat autosigné.__
__Passons maintenant à des certificats signés par une AC.__

## Avec des certificats signés par une AC

Utilisation d'une PKI _easyrsa3_.

### Initialisation

1. Utilisation de easyrsa3.
2. Utilisation de la même CA pour le client et le serveur.

 - [ ] Initialisation de la pki

```
./easyrsa init-pki

init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki

```

 - [ ] Création d'un CA

```
./easyrsa build-ca

Ignoring unknown command option: 'help'
Using SSL: openssl OpenSSL 1.1.1  11 Sep 2018

Enter New CA Key Passphrase: password
Re-Enter New CA Key Passphrase: password
Generating RSA private key, 2048 bit long modulus (2 primes)
.......................................................................+++++
................................................................................+++++
e is 65537 (0x010001)
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:ubuntu

CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/ca.crt
```

### TLS unidirectionnel - identification du serveur (one-way TLS)

On reprend le cas précédent sauf que l'on va utiliser des certificats générés par notre pki.

#### Génération des keypair

#### keypair server

```
./easyrsa --subject-alt-name='DNS:localhost,DNS:ubuntu,IP:127.0.0.1' build-server-full server
Using SSL: openssl OpenSSL 1.1.1  11 Sep 2018
Generating a RSA private key
....+++++
.......................................................+++++
writing new private key to '/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/easy-rsa-6290.VPOnv5/tmp.lrQfl4'
Enter PEM pass phrase: password
Verifying - Enter PEM pass phrase: password
-----
Using configuration from /home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/easy-rsa-6290.VPOnv5/tmp.wxN9we
Enter pass phrase for /home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/private/ca.key: password
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'server'
Certificate is to be certified until Mar 20 10:27:41 2022 GMT (825 days)

Write out database with 1 new entries
Data Base Updated
```

*Info : Oublie volontaire de l'ip de la machine (192.168.86.130) dans le "subject-alt-name".*

#### keypaire client

```
./easyrsa --subject-alt-name='DNS:localhost,DNS:ubuntu,IP:127.0.0.1' build-client-full client
Using SSL: openssl OpenSSL 1.1.1  11 Sep 2018
Generating a RSA private key
...................+++++
................+++++
writing new private key to '/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/easy-rsa-3513.MCnqqw/tmp.sNBqf5'
Enter PEM pass phrase: password
Verifying - Enter PEM pass phrase: password
-----
Using configuration from /home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/easy-rsa-3513.MCnqqw/tmp.jL0SvU
Enter pass phrase for /home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/private/ca.key: password
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'client'
Certificate is to be certified until Mar 20 09:34:52 2022 GMT (825 days)

Write out database with 1 new entries
Data Base Updated

```

*Info : Oublie volontaire de l'ip de la machine (192.168.86.130) dans le "subject-alt-name" .*

#### Serveur

 - [ ] Importer le certificat issue de la keypair client dans le truststore

```
keytool -keystore server/src/main/resources/truststore.jks -importcert -file "/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/issued/client.crt" -alias client -storepass password
```

 - [ ] Importer la keypair server dans le keystore et modifier la configuration

```
openssl pkcs12 -export -in easyrsa3/pki/issued/server.crt -inkey easyrsa3/pki/private/server.key -out server/src/main/resources/server-signed.p12 -name server -passout pass:password
Enter pass phrase for easyrsa3/pki/private/server.key: password

keytool -importkeystore -srckeystore server/src/main/resources/server-signed.p12 -srcstoretype PKCS12 -destkeystore server/src/main/resources/server.jks -srcstorepass password -deststorepass password
Importing keystore server/src/main/resources/server-signed.p12 to server/src/main/resources/server.jks...
Entry for alias client successfully imported.
Import command completed:  1 entries successfully imported, 0 entries failed or cancelled
```

__application.yml__

```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server
    key-password: password
    trust-store: classpath:truststore.jks
    trust-store-password: password
    client-auth: need
```


#### Client


 - [ ] Importer le certificat issue de la keypair serveur dans le truststore

```
    keytool -keystore client/src/main/resources/truststore.jks -importcert -file "/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/issued/server.crt" -alias server -storepass password
```

 - [ ] Importer la keypair client dans le keystore et modifier la configuration

```
openssl pkcs12 -export -in easyrsa3/pki/issued/client.crt -inkey easyrsa3/pki/private/client.key -out client/src/main/resources/client-signed.p12 -name client -passout pass:password
Enter pass phrase for easyrsa3/pki/private/client.key: password

keytool -importkeystore -srckeystore client/src/main/resources/client-signed.p12 -srcstoretype PKCS12 -destkeystore client/src/main/resources/client.jks -srcstorepass password -deststorepass password
Importing keystore client/src/main/resources/client-signed.p12 to client/src/main/resources/client.jks...
Entry for alias client successfully imported.
Import command completed:  1 entries successfully imported, 0 entries failed or cancelled
```

 [ ] Test

```
 shell:>get https://localhost:8443/api/test/alive
```

Le résulat est : `I'm alive !` c'est bon.

```
 shell:>get https://192.168.86.130:8443/api/test/alive
```

Le résultat est : `Certificate for <192.168.86.130> doesn't match any of the subject alternative names: [localhost, ubuntu, 127.0.0.1]`.
Normal c'est l'oublie volontaire de cette ip dans le "subject-alt-name" lors de la génération.
Le certificat émis par le serveur ne comporte pas cette ip dans le SAN et donc le validation du hostname ne passe pas.

### TLS bidirectionnel  - basé sur l'autorité de certification (two-way TLS)

Une autre façon de faire du MTLS est de ce baser sur la confiance de l'autorité de certification. 
Cette méthode possède des avantages et des inconvénients :

- __Avantages__
    - Les clients n'ont pas besoin d'ajouter le certificat du serveur
    - Le serveur n'a pas besoin d'ajouter tous les certificats des clients
    - La maintenance sera moindre car seule la validité du certificat de l'autorité de certification peut expirer

- __Inconvénients__
    - Plus de contrôle sur les applications autorisées à appeler votre application. 
    - Toutes les applications disposant d'un certificat signé par l'autorité de certification peuvent y accèder.

##### Serveur

 - [ ] Suppression du certificat précédent dans le truststore
 
``` 
keytool -keystore server/src/main/resources/truststore.jks -delete -alias client -storepass password
```

 - [ ] Importer le CA dans le truststore

```
keytool -keystore server/src/main/resources/truststore.jks -importcert -file "/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/ca.crt" -alias cacert -storepass password
```

 - [ ] Importer la clef dans le keystore serveur

```
openssl pkcs12 -export -in easyrsa3/pki/issued/server.crt -inkey easyrsa3/pki/private/server.key -out server/src/main/resources/server-signed.p12 -name server -passout pass:password
Enter pass phrase for easyrsa3/pki/private/server.key: password

keytool -importkeystore -srckeystore server/src/main/resources/server-signed.p12 -srcstoretype PKCS12 -destkeystore server/src/main/resources/server.jks -srcstorepass password -deststorepass password
Importing keystore server/src/main/resources/server-signed.p12 to server/src/main/resources/server.jks...
Entry for alias client successfully imported.
Import command completed:  1 entries successfully imported, 0 entries failed or cancelled
```

__application.yml__

```yaml
server:
  port: 8443
  ssl:
    enabled: on
    key-store: classpath:server.jks
    key-store-password: password
    key-alias: server
    key-password: password
    trust-store: classpath:truststore.jks
    trust-store-password: password
    client-auth: need
```

##### Client

 - [ ] Suppression du certificat précédent dans le truststore
 
```
keytool -keystore client/src/main/resources/truststore.jks -delete -alias server -storepass password
```

 - [ ] Importer le CA dans le truststore

```
keytool -keystore client/src/main/resources/truststore.jks -importcert -file "/home/npierreyves/IdeaProjects/test-tls/easyrsa3/pki/ca.crt" -alias cacert -storepass password
```

 - [ ] Importer la clef dans le keystore client

```
openssl pkcs12 -export -in easyrsa3/pki/issued/client.crt -inkey easyrsa3/pki/private/client.key -out client/src/main/resources/client-signed.p12 -name client -passout pass:password
Enter pass phrase for easyrsa3/pki/private/client.key: password

keytool -importkeystore -srckeystore client/src/main/resources/client-signed.p12 -srcstoretype PKCS12 -destkeystore client/src/main/resources/client.jks -srcstorepass password -deststorepass password
Importing keystore client/src/main/resources/client-signed.p12 to client/src/main/resources/client.jks...
Entry for alias client successfully imported.
Import command completed:  1 entries successfully imported, 0 entries failed or cancelled
```

 - [ ] Test

```
shell:>get https://localhost:8443/api/test/alive
shell:>get https://ubuntu:8443/api/test/alive
shell:>get https://127.0.0.1:8443/api/test/alive
```

Le résulat pour tout les cas est : `I'm alive !`
Par contre avec l'appel :
```
 shell:>get https://192.168.86.130:8443/api/test/alive
```

Le résultat est : `Certificate for <192.168.86.130> doesn't match any of the subject alternative names: [localhost, ubuntu, 127.0.0.1]`.
LA raison est exactement qu'énoncé précédemment.
