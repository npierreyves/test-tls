package fr.npierreyves.demo.tls.server;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/test")
public class TestController {

    @GetMapping("/alive")
    public ResponseEntity<String>  areYouAlive() {
        return ResponseEntity.ok("I'm alive !");
    }
}
